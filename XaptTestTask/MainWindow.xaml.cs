﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.Html5;
using System.Net;


namespace XaptTestTask
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Declareing webdriver and starter URL
            IWebDriver driver = new ChromeDriver();
            //IWebDriver driver = new FirefoxDriver();

            driver.Navigate().GoToUrl("https://www.wikipedia.org/");

            //Some latency
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            System.Threading.Thread.Sleep(100);

            //English language link  selection
            driver.FindElement(By.Id("js-link-box-en")).Click();

            //Some Latency
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            System.Threading.Thread.Sleep(100);

            //Giving search terms to searchbar
            driver.FindElement(By.Id("searchInput")).SendKeys("Test Automation" + Keys.Enter);

            //Some Latency
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            System.Threading.Thread.Sleep(300);

            //Searching for certain text on page -> UNIT TESTING

            if (driver.FindElement(By.XPath("//span[text()='Unit testing']")) != null)
            {
                MessageBox.Show("Yes, it's there.");
            }
            else
            {
                MessageBox.Show("No, it's not there.");
            }

            //Some Latency
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            System.Threading.Thread.Sleep(100);

            //Searching for certain image -> TEST AUTOMATION INTERFACE MODEL
            // I couldn't figure that one out. I don't know how to refer to he image just by the given keywords. 
            // Of course I could use something from the sourcecode of the website, but that's just not the point is it.

            /*foreach (var item in driver.FindElements(By.TagName("a")))
            {
                if(item.ClassName() == "image" && item.GetAttribute("href").Contains("Test Automation Interface"))
                {
                    MessageBox.Show("Jaja ott van");
                }
            }*/

            //Some Latency
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            System.Threading.Thread.Sleep(100);

            //Searching for Link and click on it
            driver.FindElement(By.LinkText("Behavior driven development")).Click();


        }
    }
}
